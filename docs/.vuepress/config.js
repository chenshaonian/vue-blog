module.exports = {
    base: '/',
    title: 'Hello, World.',
    description: '📦 🎨 A api-friendly theme for VuePress.',
    theme: 'api',
    dest: 'public',
  }